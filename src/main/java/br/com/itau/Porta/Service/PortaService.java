package br.com.itau.Porta.Service;

import br.com.itau.Porta.Exceptions.PortaNaoEncontrada;
import br.com.itau.Porta.Model.Porta;
import br.com.itau.Porta.Repository.PortaRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    PortaRespository portaRespository;


    public Porta incluirNovaPorta(Porta porta) {

        return portaRespository.save(porta);
    }


    public Porta buscarPortaPorId(int id) {

        Optional<Porta> optionalPorta = portaRespository.findById(id);

        if (optionalPorta.isPresent()) {
            Porta porta = optionalPorta.get();

            return porta;
        } else {

            throw new PortaNaoEncontrada();
        }


    }
}
