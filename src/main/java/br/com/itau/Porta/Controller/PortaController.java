package br.com.itau.Porta.Controller;


import br.com.itau.Porta.Model.Porta;
import br.com.itau.Porta.Service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class PortaController {

    @Autowired
    PortaService portaService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta incluirNovoPorta(@RequestBody @Valid Porta porta) {
        return portaService.incluirNovaPorta(porta);

    }


    @GetMapping("/{id}")
    public Porta buscarPortaPorId(@PathVariable(name = "id") int id) {

        return portaService.buscarPortaPorId(id);

    }


}
