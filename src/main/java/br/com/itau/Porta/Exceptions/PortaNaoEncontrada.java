package br.com.itau.Porta.Exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Porta nao encontrada!")
public class PortaNaoEncontrada extends RuntimeException {
}
