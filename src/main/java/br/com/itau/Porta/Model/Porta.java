package br.com.itau.Porta.Model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Porta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  int id;

    @NotNull(message = "Favor informar o Andar")
    private  int andar;

    @NotNull(message = "Favor informar a porta")
    private int sala;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAndar() {
        return andar;
    }

    public void setAndar(int andar) {
        this.andar = andar;
    }

    public int getsala() {
        return sala;
    }

    public void setsala(int  porta) {
        this.sala = porta;
    }
}
