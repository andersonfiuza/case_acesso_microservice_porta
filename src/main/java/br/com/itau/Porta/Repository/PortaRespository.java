package br.com.itau.Porta.Repository;

import br.com.itau.Porta.Model.Porta;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PortaRespository extends CrudRepository<Porta, Integer> {
}
